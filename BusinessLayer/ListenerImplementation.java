package BusinessLayer;

import DataAccessLayer.Queries;
import Model.Client;
import Model.Product;
import Presentation.ViewTable;
import com.mysql.cj.util.StringUtils;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.List;

public class ListenerImplementation {
    private int type;
    private JTextField jId;
    private JTextField jNume;
    private JTextField jCantitate;
    private JTextField jPret;
    private int id;
    private String nume;
    private int cantitate;
    private int pret;

    public ListenerImplementation(JTextField id, JTextField nume) {
        type = 1;
        this.jId = id;
        this.jNume = nume;
    }
    public ListenerImplementation(JTextField id, JTextField nume, JTextField cantitate, JTextField pret) {
        this(id, nume);
        this.jCantitate = cantitate;
        this.jPret = pret;
        type = 2;
    }
    public void addNewActionListener(JButton addNew) {
        addNew.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                convertText();
                addNew();
            }
        });
    }
    public void editActionListener(JButton edit) {
        edit.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                convertText();
                edit();
            }
        });
    }
    public void deleteActionListener(JButton edit) {
        edit.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                convertText();
                delete();
            }
        });
    }
    public void viewActionListener(JButton view) {
        view.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                convertText();
                view();
            }
        });
    }
    private void addNew() {
        if (type == 1) {
            if (Queries.findClientById(id) != null) {
                System.out.println("id taken");
            }
            Client client = new Client();
            client.setIdClient(id);
            client.setNume(nume);
            Queries.addNewClient(client);
        }
        else if (type == 2) {
            if (Queries.findProductById(id) != null) {
                System.out.println("id taken");
            }
            Product product = new Product();
            product.setIdProduct(id);
            product.setNume(nume);
            product.setStocDisponibil(cantitate);
            product.setPretBuc(pret);
            Queries.addNewProduct(product);
        }
    }
    private void edit() {
        if (type == 1) {
            Client client = Queries.findClientById(id);
            if (client != null) {
                client.setNume(nume);
                Queries.editClient(client);
            }
        }
        else if (type == 2) {
            Product product = Queries.findProductById(id);
            if (product != null) {
                product.setNume(nume);
                product.setStocDisponibil(cantitate);
                product.setPretBuc(pret);
                Queries.editProduct(product);
            }
        }
    }
    private void delete() {
        if (type == 1) {
            Client client = Queries.findClientById(id);
            if (client != null) {
                if (client.getNume() != null && client.getNume().equals(nume)) {
                    Queries.setNullClient(client);
                }
            }
            else {
                System.out.println("Data does not match");
            }
        }
        else if (type == 2) {
            Product product = Queries.findProductById(id);
            if (product != null && product.getNume() != null && product.getNume().equals(nume) && product.getPretBuc() == pret) {
                if (product.getNume().equals(nume)) {
                    Queries.setNullProduct(product);
                }
                else {
                    System.out.println("Data does not match");
                }
            }
        }
    }
    private void view() {
        List<Object> objects = new ArrayList<>();
        if (type == 1) {
            Client client = new Client();
            int clientCount = 0;
            while (client != null) {
                clientCount++;
                client = Queries.findClientById(clientCount);
                if (client != null && client.getNume() != null) {
                    objects.add(client);
                }
            }
        }
        else if (type == 2) {
            Product product = new Product();
            int productCount = 0;
            while (product != null) {
                productCount++;
                product = Queries.findProductById(productCount);
                if (product != null && product.getNume() != null) {
                    objects.add(product);
                }
            }
        }
        ViewTable viewTable = new ViewTable(objects, type*2);
        viewTable.create();
    }
    private void convertText() {
        if (type == 1) {
            if (StringUtils.isStrictlyNumeric(jId.getText())) {
                id = Integer.valueOf(jId.getText());
                nume = jNume.getText();
            }
        } else
        if (StringUtils.isStrictlyNumeric(jId.getText()) && StringUtils.isStrictlyNumeric(jCantitate.getText()) && StringUtils.isStrictlyNumeric(jPret.getText())) {
            id = Integer.valueOf(jId.getText());
            nume = jNume.getText();
            cantitate = Integer.valueOf(jCantitate.getText());
            pret = Integer.valueOf(jPret.getText());
        }
    }
}
