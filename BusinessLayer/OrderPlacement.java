package BusinessLayer;

import DataAccessLayer.Queries;
import Model.Order;
import Model.Product;
import com.mysql.cj.util.StringUtils;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class OrderPlacement {
    private JTextField jIdClient;
    private JTextField jIdProduct;
    private JTextField jCantitate;
    private int idClient;
    private int idProduct;
    private int cantitate;

    public OrderPlacement(JTextField idClient, JTextField idProduct, JTextField cantitate) {
        jIdClient = idClient;
        jIdProduct = idProduct;
        jCantitate = cantitate;
    }
    public void placeListener(JButton place) {
        place.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                convertText();
                place();
            }
        });
    }
    private void convertText() {
        if (StringUtils.isStrictlyNumeric(jIdClient.getText()) && StringUtils.isStrictlyNumeric(jCantitate.getText()) && StringUtils.isStrictlyNumeric(jIdProduct.getText())) {
            idClient = Integer.valueOf(jIdClient.getText());
            idProduct = Integer.valueOf(jIdProduct.getText());
            cantitate = Integer.valueOf(jCantitate.getText());
        }
    }
    private void place() {
        Product product = Queries.findProductById(idProduct);
        if (product != null) {
            if (product.getStocDisponibil() >= cantitate) {
                if (cantitate != 0) {
                    setOrder();
                    decStoc();
                }
            }
            else {
                System.out.println("not enough on stock");
            }
        }
        else {
            System.out.println("no such product");
        }
    }
    private void setOrder() {
        Order order = new Order();
        order.setIdOrder(getOrderCount());
        order.setIdClient(idClient);
        order.setIdProduct(idProduct);
        order.setCantitate(cantitate);
        int total = cantitate * Queries.findProductById(idProduct).getPretBuc();
        order.setTotal(total);
        Queries.placeOrder(order);
        System.out.println("order placed");
    }
    private void decStoc() {
        Product product = Queries.findProductById(idProduct);
        int stoc = product.getStocDisponibil() - cantitate;
        product.setStocDisponibil(stoc);
        Queries.editProduct(product);
    }
    private int getOrderCount() {
        int orderCount = 0;
        Order order = new Order();
        while (order != null) {
            orderCount ++;
            order = Queries.findOrderById(orderCount);
        }
        return orderCount;
    }
}
