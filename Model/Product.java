package Model;

public class Product {
    private int idProduct;
    private String nume;
    private int stocDisponibil;
    private int pretBuc;

    public void setIdProduct(int idProduct) {
        this.idProduct = idProduct;
    }
    public int getIdProduct() {
        return idProduct;
    }
    public void setNume (String nume) {
        this.nume = nume;
    }
    public String getNume() {
        return nume;
    }
    public void setStocDisponibil (int stocDisponibil) {
        this.stocDisponibil = stocDisponibil;
    }
    public int getStocDisponibil() {
        return stocDisponibil;
    }
    public void setPretBuc (int pretBuc) {
        this.pretBuc = pretBuc;
    }
    public int getPretBuc() {
        return pretBuc;
    }
}
