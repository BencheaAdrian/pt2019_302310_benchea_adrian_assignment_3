package Model;

public class Order {
    private int idOrder;
    private int idClient;
    private int idProduct;
    private int cantitate;
    private int total;

    public void setIdOrder(int idOrder) {
        this.idOrder = idOrder;
    }
    public int getIdOrder() {
        return idOrder;
    }
    public void setIdClient(int idClient) {
        this.idClient = idClient;
    }
    public int getIdClient() {
        return idClient;
    }
    public void setIdProduct(int idProduct) {
        this.idProduct = idProduct;
    }
    public int getIdProduct() {
        return idProduct;
    }
    public void setCantitate(int cantitate) {
        this.cantitate = cantitate;
    }
    public int getCantitate() {
        return cantitate;
    }
    public void setTotal(int total) {
        this.total = total;
    }
    public int getTotal() {
        return total;
    }
}
