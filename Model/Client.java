package Model;

public class Client {
    private int idClient;
    private String nume;

    public void setIdClient(int idClient) {
        this.idClient = idClient;
    }
    public int getIdClient() {
        return idClient;
    }
    public void setNume(String nume) {
        this.nume = nume;
    }
    public String getNume() {
        return nume;
    }
}
