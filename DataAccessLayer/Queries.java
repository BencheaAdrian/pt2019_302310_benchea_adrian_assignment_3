package DataAccessLayer;

import Model.Client;
import Model.Order;
import Model.Product;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class Queries {
    private static final String FIND_CLIENT_BY_ID = "SELECT * FROM client WHERE idClient = ?";
    private static final String FIND_PRODUCT_BY_ID = "SELECT * FROM product WHERE idProduct = ?";
    private static final String FIND_ORDER_BY_ID = "SELECT * FROM `order` WHERE idOrder = ?";
    private static final String PLACE_ORDER = "INSERT INTO `order` " + "VALUES (?, ?, ?, ?, ?);";
    private static final String ADD_CLIENT = "INSERT INTO client " + "VALUES (?, ?);";
    private static final String ADD_PRODUCT = "INSERT INTO product " + "VALUES (?, ?, ?, ?);";
    private static final String EDIT_CLIENT = "UPDATE client\n" +
            "SET nume = ?\n" + "WHERE idClient = ?;";
    private static final String EDIT_PRODUCT = "UPDATE product\n" +
            "SET nume = ?, stocDisponibil = ?, pretBuc = ?\n" + "WHERE idProduct = ?;";
    private static final String SET_NULL_CLIENT = "UPDATE client\n" +
            "SET nume = NULL\n" + "WHERE idClient = ?;";
    private static final String SET_NULL_PRODUCT = "UPDATE product\n" +
            "SET nume = NULL, stocDisponibil = NULL, pretBuc = NULL\n" + "WHERE idProduct = ?;";

    public static Client findClientById(int id) {
        Client toReturn = null;
        try {
            Connection connection = DataConnection.getConnection();
            PreparedStatement findStatement = null;
            ResultSet resultSet = null;
            findStatement = connection.prepareStatement(FIND_CLIENT_BY_ID);
            findStatement.setLong(1, id);
            resultSet = findStatement.executeQuery();
            if (resultSet.next()) {
                int idClient = resultSet.getInt("idClient");
                String nume = resultSet.getString("nume");
                toReturn = new Client();
                toReturn.setIdClient(idClient);
                toReturn.setNume(nume);
            }
            DataConnection.close(resultSet);
            DataConnection.close(findStatement);
            DataConnection.close(connection);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return toReturn;
    }
    public static Product findProductById(int id) {
        Product toReturn = null;
        try {
            Connection connection = DataConnection.getConnection();
            PreparedStatement findStatement = null;
            ResultSet resultSet = null;
            findStatement = connection.prepareStatement(FIND_PRODUCT_BY_ID);
            findStatement.setLong(1, id);
            resultSet = findStatement.executeQuery();
            if (resultSet.next()) {
                int idProduct = resultSet.getInt("idProduct");
                String nume = resultSet.getString("nume");
                int stocDisponibil = resultSet.getInt("stocDisponibil");
                int pretBuc = resultSet.getInt("pretBuc");
                toReturn = new Product();
                toReturn.setIdProduct(idProduct);
                toReturn.setNume(nume);
                toReturn.setStocDisponibil(stocDisponibil);
                toReturn.setPretBuc(pretBuc);
            }
            DataConnection.close(resultSet);
            DataConnection.close(findStatement);
            DataConnection.close(connection);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return toReturn;
    }
    public static Order findOrderById(int id) {
        Order toReturn = null;
        try {
            Connection connection = DataConnection.getConnection();
            PreparedStatement findStatement = null;
            ResultSet resultSet = null;
            findStatement = connection.prepareStatement(FIND_ORDER_BY_ID);
            findStatement.setLong(1, id);
            resultSet = findStatement.executeQuery();
            if (resultSet.next()) {
                int idOrder = resultSet.getInt("idOrder");
                int idClient = resultSet.getInt("idClient");
                int idProduct = resultSet.getInt("idProduct");
                int cantitate = resultSet.getInt("cantitate");
                int total = resultSet.getInt("total");
                toReturn = new Order();
                toReturn.setIdOrder(idOrder);
                toReturn.setIdClient(idClient);
                toReturn.setIdProduct(idProduct);
                toReturn.setCantitate(cantitate);
                toReturn.setTotal(total);
            }
            DataConnection.close(resultSet);
            DataConnection.close(findStatement);
            DataConnection.close(connection);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return toReturn;
    }
    public static void placeOrder(Order order) {
        try {
            Connection connection = DataConnection.getConnection();
            PreparedStatement findStatement = null;
            int op;
            findStatement = connection.prepareStatement(PLACE_ORDER);
            findStatement.setLong(1, order.getIdOrder());
            findStatement.setLong(2, order.getIdClient());
            findStatement.setLong(3, order.getIdProduct());
            findStatement.setLong(4, order.getCantitate());
            findStatement.setLong(5, order.getTotal());
            op = findStatement.executeUpdate();
            DataConnection.close(findStatement);
            DataConnection.close(connection);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
    public static void addNewClient(Client client) {
        if (findClientById(client.getIdClient()) == null) {
            try {
                Connection connection = DataConnection.getConnection();
                PreparedStatement findStatement = null;
                int op;
                findStatement = connection.prepareStatement(ADD_CLIENT);
                findStatement.setLong(1, client.getIdClient());
                findStatement.setString(2, client.getNume());
                op = findStatement.executeUpdate();
                DataConnection.close(findStatement);
                DataConnection.close(connection);
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
    }
    public static void addNewProduct(Product product) {
        if (findProductById(product.getIdProduct()) == null) {
            try {
                Connection connection = DataConnection.getConnection();
                PreparedStatement findStatement = null;
                int op;
                findStatement = connection.prepareStatement(ADD_PRODUCT);
                findStatement.setLong(1, product.getIdProduct());
                findStatement.setString(2, product.getNume());
                findStatement.setLong(3, product.getStocDisponibil());
                findStatement.setLong(4, product.getPretBuc());
                op = findStatement.executeUpdate();
                DataConnection.close(findStatement);
                DataConnection.close(connection);
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
    }
    public static void editClient(Client client) {
        try {
            Connection connection = DataConnection.getConnection();
            PreparedStatement findStatement = null;
            int op;
            findStatement = connection.prepareStatement(EDIT_CLIENT);
            findStatement.setString(1, client.getNume());
            findStatement.setLong(2, client.getIdClient());
            op = findStatement.executeUpdate();
            DataConnection.close(findStatement);
            DataConnection.close(connection);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
    public static void editProduct(Product product) {
        try {
            Connection connection = DataConnection.getConnection();
            PreparedStatement findStatement = null;
            int op;
            findStatement = connection.prepareStatement(EDIT_PRODUCT);
            findStatement.setString(1, product.getNume());
            findStatement.setLong(2, product.getStocDisponibil());
            findStatement.setLong(3, product.getPretBuc());
            findStatement.setLong(4, product.getIdProduct());
            op = findStatement.executeUpdate();
            DataConnection.close(findStatement);
            DataConnection.close(connection);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
    public static void setNullClient(Client client) {
        try {
            Connection connection = DataConnection.getConnection();
            PreparedStatement findStatement = null;
            int op;
            findStatement = connection.prepareStatement(SET_NULL_CLIENT);
            findStatement.setLong(1, client.getIdClient());
            op = findStatement.executeUpdate();
            DataConnection.close(findStatement);
            DataConnection.close(connection);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
    public static void setNullProduct(Product product) {
        try {
            Connection connection = DataConnection.getConnection();
            PreparedStatement findStatement = null;
            int op;
            findStatement = connection.prepareStatement(SET_NULL_PRODUCT);
            findStatement.setLong(1, product.getIdProduct());
            op = findStatement.executeUpdate();
            DataConnection.close(findStatement);
            DataConnection.close(connection);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
}
