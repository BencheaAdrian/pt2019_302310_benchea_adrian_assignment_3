package Presentation;

import javax.swing.*;
import javax.swing.table.DefaultTableModel;
import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.List;

public class ViewTable {
    private JFrame jFrame = new JFrame("View Table");
    private JPanel jPanel = new JPanel();
    private JTable jTable;
    private DefaultTableModel model;
    private List<Object> objects;
    private List<String> columns;
    private int colCount;

    public ViewTable(List<Object> objects, int colCount) {
        model = new DefaultTableModel(0, colCount);
        jTable = new JTable(model);
        this.objects = objects;
        this.colCount = colCount;
    }
    public void create() {
        setTable();
        jPanel.add(jTable);
        jFrame.add(jPanel);
        jFrame.setVisible(true);
        jFrame.pack();
    }
    private void setTable() {
        Object object;
        if (!objects.isEmpty()) {
            object = objects.get(0);
        } else return;
        retrieveProperties(object);
        for (int i = 0; i < colCount; i ++) {
            jTable.getTableHeader().getColumnModel().getColumn(i).setHeaderValue(columns.get(i));
        }
        for (Object object1: objects) {
            addElement(object1);
        }
    }
    private void retrieveProperties(Object object) {
        columns = new ArrayList<>();
        for (Field field : object.getClass().getDeclaredFields()) {
            field.setAccessible(true);
            columns.add(field.getName());
        }
    }
    private void addElement(Object object) {
        Object[] element = new Object[colCount];
        int i = -1;
        for (Field field : object.getClass().getDeclaredFields()) {
            field.setAccessible(true);
            Object val;
            i ++;
            try {
                val = field.get(object);
                element[i] = val;

            } catch (IllegalAccessException e) {
                e.printStackTrace();
            }
        }
        model.addRow(element);
    }
}
