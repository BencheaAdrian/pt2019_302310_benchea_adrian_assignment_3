package Presentation;

public class Main {
    public static void main(String[] args) {
        ClientOperations clientOperations = new ClientOperations();
        clientOperations.create();
        ProductOperations productOperations = new ProductOperations();
        productOperations.create();
        OrderEdit orderEdit = new OrderEdit();
        orderEdit.create();
    }
}
