package Presentation;

import BusinessLayer.OrderPlacement;

import javax.swing.*;

public class OrderEdit {
    private JFrame jFrame = new JFrame("Place Order");
    private JPanel jPanel = new JPanel();
    private JPanel jPanel2 = new JPanel();
    private JTextField idClient = new JTextField("client id (int)");
    private JTextField idProduct = new JTextField("product id (int)");
    private JTextField cantitate = new JTextField("cantitate (int)");
    private JButton place = new JButton("place");

    public void create() {
        OrderPlacement orderPlacement = new OrderPlacement(idClient, idProduct, cantitate);
        orderPlacement.placeListener(place);
        jPanel2.setLayout(new BoxLayout(jPanel2, BoxLayout.X_AXIS));
        jPanel2.add(place);
        jPanel.setLayout(new BoxLayout(jPanel, BoxLayout.Y_AXIS));
        jPanel.add(idClient);
        jPanel.add(idProduct);
        jPanel.add(cantitate);
        jPanel.add(jPanel2);
        jFrame.add(jPanel);
        jFrame.setVisible(true);
        jFrame.pack();
    }
}
