package Presentation;

import BusinessLayer.ListenerImplementation;

import javax.swing.*;

public class ProductOperations {
    private JFrame jFrame = new JFrame("Product Operations");
    private JPanel jPanel = new JPanel();
    private JPanel jPanel2 = new JPanel();
    private JTextField id = new JTextField("id(int)");
    private JTextField nume = new JTextField("nume(String)");
    private JTextField cantitate = new JTextField("cantitate(int)");
    private JTextField pret = new JTextField("pret(int)");
    private JButton addNew = new JButton("add new");
    private JButton edit = new JButton("edit");
    private JButton delete = new JButton("delete");
    private JButton view = new JButton("view");

    public void create() {
        ListenerImplementation listenerImplementation = new ListenerImplementation(id, nume, cantitate, pret);
        listenerImplementation.addNewActionListener(addNew);
        listenerImplementation.editActionListener(edit);
        listenerImplementation.deleteActionListener(delete);
        listenerImplementation.viewActionListener(view);
        jPanel2.setLayout(new BoxLayout(jPanel2, BoxLayout.X_AXIS));
        jPanel2.add(addNew);
        jPanel2.add(edit);
        jPanel2.add(delete);
        jPanel2.add(view);
        jPanel.setLayout(new BoxLayout(jPanel, BoxLayout.Y_AXIS));
        jPanel.add(id);
        jPanel.add(nume);
        jPanel.add(cantitate);
        jPanel.add(pret);
        jPanel.add(jPanel2);
        jFrame.add(jPanel);
        jFrame.setVisible(true);
        jFrame.pack();
    }
}
